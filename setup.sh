#!/usr/bin/env bash

command1="eval $(minikube docker-env)"
echo $command1
$command1

command2="kubectl run hello-world --image=hello-world --port=80 --image-pull-policy=Never"
echo $command2
$command2
